import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poc/domain/entities/product.dart';

class CardProduct extends StatelessWidget {
  final Product product;
  const CardProduct(this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      margin: const EdgeInsets.all(8),
      child: SizedBox(
        height: 120,
        child: Row(
          children: [
            Center(
              child: Image.network(
                product.image,
              ),
            ),
            Column(children: [
              Text(
                product.name,
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
              ),
              Text(
                product.description,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
              ),
            ])
          ],
        ),
      ),
    );
  }
}
