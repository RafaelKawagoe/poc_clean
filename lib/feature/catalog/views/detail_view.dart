import 'package:flutter/cupertino.dart';
import 'package:poc/components/app_scaffold.dart';
import 'package:poc/domain/entities/product.dart';
import 'package:poc/domain/repositories/backend_repository.dart';
import 'package:poc/domain/use_cases/get_details.dart';
import 'package:poc/feature/catalog/repositories/mock_backend_repository.dart';

class DetailView extends StatefulWidget {
  final num index;
  const DetailView({Key? key, required this.index}) : super(key: key);

  @override
  State<DetailView> createState() => _DetailViewState();
}

class _DetailViewState extends State<DetailView> {
  Product? product;

  void getDetailProduct() async {
    var produto =
        await GetDetails(MockBackendRepository()).call(widget.index).first;
    setState(() {
      product = produto;
    });
  }

  @override
  void initState() {
    super.initState();
    getDetailProduct();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      product != null
          ? Column(
              children: [
                Text(product!.name),
                Text(product!.description),
                Text(product!.valor),
                Image.network(
                  product!.image,
                  height: 200,
                ),
              ],
            )
          : Container(),
    );
  }
}
