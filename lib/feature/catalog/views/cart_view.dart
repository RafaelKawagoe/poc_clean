import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poc/components/app_card_product.dart';
import 'package:poc/components/app_scaffold.dart';
import 'package:poc/domain/entities/product.dart';
import 'package:poc/domain/use_cases/get_cart.dart';
import 'package:poc/domain/use_cases/get_products.dart';
import 'package:poc/feature/catalog/repositories/mock_backend_repository.dart';

class CartView extends StatefulWidget {
  const CartView({Key? key}) : super(key: key);

  @override
  State<CartView> createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  List<Product> _produtos = [];

  void getProducts() async {
    final getCart = GetCart(MockBackendRepository());
    final products = await getCart().where((list) => list.isNotEmpty).first;
    setState(() {
      _produtos = products;
    });
  }

  void removeFromCart(num id) async {
    final getCart = GetCart(MockBackendRepository());
    getCart.removeFromCart(id);
  }

  @override
  void initState() {
    super.initState();
    getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(ListView.builder(
        itemCount: _produtos.length,
        itemBuilder: (BuildContext context, int index) {
          return index != null
              ? Row(
                  children: [
                    CardProduct(_produtos[index]),
                    IconButton(
                      icon: Icon(Icons.remove),
                      onPressed: () {
                        removeFromCart(_produtos[index].id);
                      },
                    )
                  ],
                )
              : Container();
        }));
  }
}
