import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poc/components/app_card_product.dart';
import 'package:poc/components/app_scaffold.dart';
import 'package:poc/domain/entities/product.dart';
import 'package:poc/domain/use_cases/get_products.dart';
import 'package:poc/feature/catalog/repositories/mock_backend_repository.dart';
import 'package:poc/feature/catalog/views/cart_view.dart';

import 'detail_view.dart';

class CatalogView extends StatefulWidget {
  const CatalogView({Key? key}) : super(key: key);

  @override
  _CatalogViewState createState() => _CatalogViewState();
}

class _CatalogViewState extends State<CatalogView> {
  List<Product> _produtos = [];

  void getProducts() async {
    final getProducts = GetProducts(MockBackendRepository());
    final products = await getProducts().where((list) => list.isNotEmpty).first;
    setState(() {
      _produtos = products;
    });
  }

  @override
  void initState() {
    super.initState();
    getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      Center(
        child: Column(
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CartView()));
                },
                child: Text('carrinho')),
            Expanded(
              child: ListView.builder(
                itemCount: _produtos.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    DetailView(index: _produtos[index].id)));
                      },
                      child: CardProduct(_produtos[index]));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
