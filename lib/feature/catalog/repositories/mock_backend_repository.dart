import 'dart:async';

import 'package:poc/domain/entities/product.dart';
import 'package:poc/domain/repositories/backend_repository.dart';

class MockBackendRepository implements BackEndRepositoryInterface {
  final List<Product> products = <Product>[];

  @override
  Stream<List<Product>> getProducts() async* {
    yield [];
    await Future.delayed(const Duration(seconds: 2));
    yield [
      Product(
        id: 1,
        name: 'Nescau',
        description: 'Nescau 2.0 Nestle',
        image:
            'https://http2.mlstatic.com/D_NQ_NP_693100-MLB31841990672_082019-O.jpg',
        valor: '20,00',
      ),
      Product(
        id: 2,
        name: 'Coca Cola',
        description: 'Coca cola 2 L',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '8,02',
      ),
      Product(
        id: 1,
        name: 'Arroz',
        description: 'Arroz Tio Urbano 1-kg',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '5,23',
      ),
      Product(
        id: 2,
        name: 'Açucar',
        description: 'Açucar União 1-kg',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '3,26',
      ),
      Product(
        id: 1,
        name: 'Suco',
        description: 'Suco Tang',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refresco_em_p_de_laranja_tang_25g_-_caixa_com_15_unidades_1.jpg?v=1',
        valor: '0,80',
      ),
      Product(
        id: 2,
        name: 'Leite',
        description: 'Leite itambe',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '3,62',
      ),
      Product(
        id: 1,
        name: 'Feijão',
        description: 'Feijão 1-kg',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '5,25',
      ),
      Product(
        id: 2,
        name: 'Queijo',
        description: 'Queijo mussarela',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '12,00',
      ),
      Product(
        id: 1,
        name: 'Papel',
        description: 'Papel Higienico',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '3,26',
      ),
      Product(
        id: 2,
        name: 'Batata',
        description: 'flocos para pure de batata',
        image:
            'https://static.distribuidoracaue.com.br/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/r/e/refrigerante-coca-cola-2-litros.jpg?v=1',
        valor: '20',
      ),
    ];
  }

  @override
  Stream<Product> getDetails(num id) {
    // TODO: implement getDetails
    throw UnimplementedError();
  }

  @override
  Stream<List<Product>> getCart() async* {
    yield products;
  }

  @override
  void addToCart(Product product) {
    products.add(product);
  }

  @override
  void removeFromCart(num id) {
    products.removeWhere((element) => element.id == id);
  }
}
