import 'package:poc/domain/entities/product.dart';
import 'package:poc/domain/repositories/backend_repository.dart';

class GetCart {
  final BackEndRepositoryInterface repository;
  GetCart(this.repository);
  Stream<List<Product>> call() {
    return repository.getCart();
  }

  void addToCart(Product product) {
    repository.addToCart(product);
  }

  void removeFromCart(num id) {
    repository.removeFromCart(id);
  }
}
