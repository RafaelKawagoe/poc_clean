import 'package:poc/domain/entities/product.dart';
import 'package:poc/domain/repositories/backend_repository.dart';
import 'package:poc/domain/use_cases/get_products.dart';
import 'package:poc/feature/catalog/repositories/mock_backend_repository.dart';

class GetDetails {
  final BackEndRepositoryInterface repository;

  GetDetails(this.repository);

  Stream<Product> call(num id) {
    final getProducts = repository.getProducts().asBroadcastStream();
    print(getProducts.length);
    return getProducts
        .where((element) =>
            element.where((element) => element.id == id).isNotEmpty)
        .map((event) => event.first);
  }
}
