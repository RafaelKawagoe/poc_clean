import 'package:poc/domain/entities/product.dart';
import 'package:poc/domain/repositories/backend_repository.dart';

class GetProducts {
  final BackEndRepositoryInterface repository;

  GetProducts(this.repository);

  Stream<List<Product>> call() {
    return repository.getProducts();
  }
}
