import 'package:poc/domain/entities/product.dart';

abstract class BackEndRepositoryInterface {
  Stream<List<Product>> getProducts();
  Stream<List<Product>> getCart();

  Stream<Product> getDetails(num id);
  void addToCart(Product product);
  void removeFromCart(num id);
}
